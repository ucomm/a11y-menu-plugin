// require('a11y-menu/src/scss/style.scss');

import Navigation from 'a11y-menu';

const mainMenu = document.getElementById('main-menu');

const menuOpts = {
  fontAwesome: true,
  chevronDown: '\\f078',
  chevronUp: '\\f077'
}

const navigation = new Navigation(menuOpts);

document.addEventListener('DOMContentLoaded', () => {
  console.log(a11yOpts, 'a11yopts');
  navigation.init(mainMenu)
})