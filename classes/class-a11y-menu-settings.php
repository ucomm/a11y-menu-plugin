<?php

class A11y_Menu_Settings {
  public function __construct() {
    $this->slug = 'a11y-menu-plugin';
    $this->options = get_option('a11y_menu_options');
  }
  // create the settings page under the WP general settings area.
  public function create_settings_page() {
    $page_title = 'A11Y Menu Settings';
    $menu_title = 'A11Y Menu Settings';
    $cap = 'manage_options';
    $callback = array($this, 'settings_page_content');
    add_options_page(
      $page_title, 
      $menu_title, 
      $cap, 
      $this->slug, 
      $callback
    );
  }
  // include the form markup for the page.
  public function settings_page_content() {
    include_once(A11y_MENU_PATH . '/partials/settings-page-content.php');
  }
  // create sections for the page as needed.
  public function create_sections() {
    $sections = array(
      array(
        'id' => 'a11y_menu_section_1',
        'label' => 'Section Title',
        'callback' => array($this, 'section_callback'),
      )
    );

    foreach($sections as $index => $section) {
      add_settings_section(
        $section['id'],
        $section['label'],
        $section['callback'],
        $this->slug
      );
    }
  }
  public function section_callback($arguments) {
    switch($arguments['id']) {
      case 'a11y_menu_section_1':
        include_once(A11y_MENU_PATH . '/partials/sections/section-1.php');
        break;
    }
  }
  public function create_fields() {
    $fields = array(
      array(
        'id' => 'text_1',
        'label' => 'First Field',
        'section' => 'a11y_menu_section_1',
        'type' => 'text',
        'options' => false,
        'placeholder' => 'enter some text',
        'helper' => 'helper text',
        'supplemental' => 'supplemental text',
        'default' => false
      ),
      array(
        'id' => 'text_2',
        'label' => 'Second Field',
        'section' => 'a11y_menu_section_1',
        'type' => 'text',
        'options' => false,
        'placeholder' => 'enter some text',
        'helper' => 'helper text',
        'supplemental' => 'supplemental text',
        'default' => false
      ),
      array(
        'id' => 'select',
        'label' => 'Select Field',
        'section' => 'a11y_menu_section_1',
        'type' => 'select',
        'options' => array(
          'yes' => 'YES!',
          'no' => 'NEVER!!!',
          'maybe' => 'SOMETIMES!?'
        ),
        'default' => 'maybe'
      )
    );

    foreach ($fields as $index => $field) {
      add_settings_field(
        $field['id'],
        $field['label'],
        array($this, 'field_callback'),
        $this->slug,
        $field['section'],
        $field
      );
    }
    register_setting($this->slug, 'a11y_menu_options');
  }
  public function field_callback($arguments) {

    $id = $arguments['id'];

    $options = get_option('a11y_menu_options');

    $value = $options[$id];


    switch ($arguments['type']) {
      case 'select':
        include(A11y_MENU_PATH . '/partials/inputs/select-input.php');
        break;
      case 'text':
        include(A11y_MENU_PATH . '/partials/inputs/text-input.php');
        break;
      
    }
  }
  public function test_sanitize_callback($input) {
    return $input;
  }
  public function init() {

    $option_defaults = array(
      'text_1' => '',
      'text_2' => '',
      'select' => 'maybe'
    );
    add_option('a11y_menu_options', $option_defaults);


    add_action('admin_menu', array($this, 'create_settings_page'));
    add_action('admin_init', array($this, 'create_sections'));
    add_action('admin_init', array($this, 'create_fields'));
  }
}

$settings = new A11y_Menu_Settings();
$settings->init();